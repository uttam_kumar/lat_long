package com.example.hp.distancelatlong;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.FloatMath;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private TextView distanceTextView, shortestDistanceTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        distanceTextView = findViewById(R.id.textViewId);
        shortestDistanceTextView=findViewById(R.id.shortTextViewId);
        double lat1=22.845641;
        double lon1=89.540328;
        double lat2=23.810296;
        double lon2=90.412658;


        double distance_m;
        distance_m = calculateDistance( lat1, lon1,lat2, lon2);
        double distance_km = distance_m / 1000*1.62;
        distanceTextView.setText(distance_km + " Km");



        double shortest_distance = calculateShortestDistance( lat1, lon1,lat2, lon2);
        shortestDistanceTextView.setText(shortest_distance+"");
    }


    /**
     * distance between two points....
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return
     */
    private double calculateDistance(double lat1, double lng1, double lat2, double lng2){
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;



        return dist;
    }

    /**
     * Shortest distance between two points
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return
     */
    private double calculateShortestDistance(double lat1, double lng1, double lat2, double lng2){
        Location locationA = new Location("point A");

        locationA.setLatitude(lat1);
        locationA.setLongitude(lng1);

        Location locationB = new Location("point B");

        locationB.setLatitude(lat2);
        locationB.setLongitude(lng2);
       return locationA.distanceTo(locationB)*1.61/1000;
    }

}
